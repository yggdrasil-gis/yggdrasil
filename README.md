# YGGDRASIL #



### What is YGGDRASIL? ###

**YGGDRASIL** (**Y**our **G**uide to **G**eospatial **D**ata, **R**esearch, & **A**nalysis for **S**cientific **I**nteractive **L**iteracy) 
is a 3D world tree map consisting of informatics overlays that display visualizations of statistical & ecological data in real time.